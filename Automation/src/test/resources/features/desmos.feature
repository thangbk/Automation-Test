Feature:
  Scenario Outline: <op> two numbers
    Given Visit page desmos.com
    When Enter the number1 <number_1>
    And Enter the operator <op>
    And Enter the number2 <number_2>
    Then the result should be <result>
    Examples:
      | number_1 | op       | number_2 | result |
      | 1        | +        | 2        | 3      |
      | 3        | -        | 1        | 2      |
      | 2        | *        | 3        | 6      |
      | 6        | /        | 2        | 3      |
      | 62        | /        | 2        | 31      |
      | 6.2        | +        | 2        | 8.2      |
      | 2        | /        | 0        | undefined      |

