package automation.util;

import java.io.FileReader;
import java.util.Properties;

public class Util
{
    public static Properties loadConfig(String configFilePath) throws Exception
    {
        Properties properties = new Properties();
        FileReader fileReader = new FileReader(configFilePath);
        properties.load(fileReader);
        return properties;
    }
}
