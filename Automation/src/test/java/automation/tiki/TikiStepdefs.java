//
//  TikiStepdefs.java
//  Automation
//
//  Created by Thang Nguyen Manh on 05/22/20.
//  Copyright © 2020 TEASOFT. All rights reserved.
//

package automation.tiki;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class TikiStepdefs
{
    private final WebDriver driver;

    private List<String> orderNumbers = new ArrayList<>();
    private final WebDriverWait wait;

    private final By bySearchInput = By.cssSelector("input[class^='FormSearch__Input']");
    private final By bySearchButton = By.cssSelector("button[class^='FormSearch__Button']");
    private final By byProductItem = By.cssSelector("a[class='search-a-product-item']");
    private final By byAddCartButton = By.cssSelector("button[class='btn btn-add-to-cart']");
    private final By byCartSubmitButton = By.cssSelector("button[class='cart__submit']");
    private final By byUsername = By.cssSelector("span[class^='Userstyle__NoWrap']");
    private final By byConfirmAddressButton = By.cssSelector("button[class='btn saving-address']");
    private final By byOrderButton = By.cssSelector("div[class='order-button']");
    private final By byOrderNumber = By.cssSelector("div[class^='order-number']");
    private final By byCancelOrderButton = By.cssSelector("a[class='cancel-order']");



    public TikiStepdefs() {
        driver = Hook.driver;
        wait = new WebDriverWait(driver, 20);
    }

    @Given("Go to Tiki.vn")
    public void go_to_tikivn() {
        driver.get("https://tiki.vn");
    }

    @When("^Search item$")
    public void search(List<String> keywords) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(bySearchInput));
        String keyword = System.getProperty("keyword");
        if (keyword == null || keyword.length() == 0) {
            Random random = new Random();
            int rand = random.nextInt(keywords.size() - 1);
            keyword = keywords.get(rand);
        }
        WebElement inputSearch = driver.findElement(bySearchInput);
        inputSearch.sendKeys(keyword);

        WebElement buttonSearch = driver.findElement(bySearchButton);
        buttonSearch.sendKeys(Keys.ENTER);
    }

    @And("^Visit item detail page$")
    public void visit_item_detail_page() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(byProductItem));
        WebElement productItem = driver.findElement(byProductItem);
        if (productItem == null) {
            throw new Exception("Item not found");
        }

        String itemUrl = productItem.getAttribute("href");
        driver.navigate().to(itemUrl);
    }

    @And("^Add item to cart$")
    public void add_items_to_cart() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(byAddCartButton));
        int count = 2;
        String itemCount = System.getProperty("count");
        try {
            count = Integer.parseInt(itemCount);
        } catch(Exception ignored) {}

        if(count > 1) {
            WebElement itemCountInput = driver.findElement(By.cssSelector("input[class='input']"));
            itemCountInput.clear();
            itemCountInput.sendKeys(String.valueOf(count));
        }

        WebElement addCartButton = driver.findElement(byAddCartButton);
        addCartButton.sendKeys(Keys.ENTER);
    }

    @And("^Visit to Cart page$")
    public void visit_to_cart_page() {
        driver.navigate().to("https://tiki.vn/checkout/cart");
    }

    @And("^Process the order$")
    public void process_the_order() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(byCartSubmitButton));
        WebElement cartSubmitButton = driver.findElement(byCartSubmitButton);
        cartSubmitButton.sendKeys(Keys.ENTER);
    }

    @And("^Do login if Login Screen showed$")
    public void login_with_if_login_screen_showed() throws Throwable {
        Thread.sleep(1000);

        WebElement usernameLabel = driver.findElement(byUsername);
        if (usernameLabel == null) {
            return;
        }

        // Login
        WebElement inputUsername = driver.findElement(By.cssSelector("input[id='email']"));
        WebElement inputPassword = driver.findElement(By.cssSelector("input[id='password']"));

        String username = System.getProperty("user");
        String password = System.getProperty("password");

        if (username == null || username.length() == 0 || password == null || password.length() == 0) {
            throw new Exception("User or Password must not empty");
        }

        inputUsername.sendKeys(username);
        inputPassword.sendKeys(password);

        WebElement buttonLogin = null;
        List<WebElement> buttons = driver.findElements(By.cssSelector("button[class^='UserModalstyle__RightButton']"));
        for(WebElement button: buttons) {
            if (button.getText().equalsIgnoreCase("Đăng nhập")) {
                buttonLogin = button;
                break;
            }
        }

        if(buttonLogin == null) {
            throw new Exception("Login Button not found");

        }
        buttonLogin.sendKeys(Keys.ENTER);
    }

    @And("^Select Shipping address$")
    public void select_shipping_address() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(byConfirmAddressButton));
        Thread.sleep(2000);
        WebElement confirmAddressButton = driver.findElement(byConfirmAddressButton);
        if(confirmAddressButton == null) {
            // Nhập địa chỉ mới
            throw new Exception("Do not have a shipping address");
        }

        confirmAddressButton.sendKeys(Keys.ENTER);
    }

    @And("^Order$")
    public void order() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(byOrderButton));
        WebElement divOrderButton = driver.findElement(byOrderButton);
        WebElement orderButton = divOrderButton.findElement(By.cssSelector("button"));
        orderButton.sendKeys(Keys.ENTER);
    }

    @Then("^show order number$")
    public void show_order_number() throws Throwable {
        wait.until(ExpectedConditions.visibilityOfElementLocated(byOrderNumber));
        List<WebElement> divOrderNumbers = driver.findElements(byOrderNumber);
        if(divOrderNumbers == null) {
            throw new Exception("Order failed");
        }

        System.out.println("Order successful");
        for (WebElement e: divOrderNumbers) {
            String orderNumber = e.getText();
            System.out.println("Order number: " + orderNumber);
            orderNumbers.add(orderNumber);
        }
    }

    public void cancel_orders() {
        if(orderNumbers.size() == 0) {
            return;
        }

        cancel_order(orderNumbers.get(0));
//        System.out.println("Order successful");
//        System.out.println("Order number: " + orderNumber);
    }

    public void cancel_order(String orderNumber) {
        driver.navigate().to("https://tiki.vn/sales/order/view/" + orderNumber);
        wait.until(ExpectedConditions.visibilityOfElementLocated(byCancelOrderButton));
        WebElement cancelOrderButton = driver.findElement(byCancelOrderButton);
        cancelOrderButton.sendKeys(Keys.ENTER);

        WebElement selectReason = driver.findElement(By.cssSelector("select"));
        selectReason.sendKeys(Keys.ENTER);
        for(int i=0; i<8; i++) {
            selectReason.sendKeys(Keys.ARROW_DOWN);
        }

        WebElement textReason = driver.findElement(By.cssSelector("textarea"));
        textReason.sendKeys("Test");

        WebElement buttonSubmit = driver.findElement(By.cssSelector("button[class^='submit']"));
        buttonSubmit.sendKeys(Keys.ENTER);
    }

    @After
    public void finish() {
        cancel_orders();
        driver.close();
        driver.quit();
    }
}
