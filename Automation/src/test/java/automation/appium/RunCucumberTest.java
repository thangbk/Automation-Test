package automation.appium;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty","html:target/report"},
        features = "src/test/resources/features/sikuli.feature",
        glue = {"automation.appium"}
)
public class RunCucumberTest {
}


