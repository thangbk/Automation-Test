package automation.appium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import java.net.URL;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions {

    private WebDriver driver;

    @Before
    public void init() {
//        System.setProperty("webdriver.chrome.driver", "/Users/ThangBK/Downloads/chromedriver");
//        driver = new ChromeDriver();
    }

    public void sikuli() throws FindFailed
    {
        Screen screen = new Screen();
        Pattern pattern = new Pattern("/Users/ThangBK/Documents/Programming/Java/Workspace/Automation/src/test/resources/images/vscode.png");
        screen.click(pattern);
    }

    @Given("^given$")
    public void given() throws Throwable {
//        sikuli();

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.0");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "17b3caff");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.CHROME);

        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        AppiumDriver driver = new AndroidDriver(url, capabilities);
        String sessionId = driver.getSessionId().toString();
        System.out.println(sessionId);

    }

    @When("^when$")
    public void when() throws Throwable {
    }

    @Then("^Then$")
    public void then() throws Throwable {
    }

    @After
    public void finish(Scenario scenario) {
//        scenario.embed();
//        driver.close();
    }
}
