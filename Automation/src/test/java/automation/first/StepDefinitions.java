package automation.first;

import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions {

    private WebDriver driver;

    @Before
    public void init() {
//        System.setProperty("webdriver.chrome.driver", "/Users/ThangBK/Downloads/chromedriver");
//        driver = new ChromeDriver();
    }

    public void sikuli() throws FindFailed
    {
        Screen screen = new Screen();
        Pattern pattern = new Pattern("src/test/resources/images/xcode.png");
        screen.click(pattern);
    }

    @Given("The login screen is displayed on browser")
    public void the_login_screen_is_displayed_on_browser() throws FindFailed
    {
//        driver.get("http://testmaster.vn/Account/login");
        sikuli();
    }

    @When("^The user attempt to login with (.+) and invalid (.+)$")
    public void the_user_attempt_to_login_with_and_invalid(String username, String password) throws Throwable {
//        WebElement inputLoginEmail = driver.findElement(By.cssSelector("input[type='email']"));
//        WebElement inputLoginPassword = driver.findElement(By.cssSelector("input[type='password']"));
//        WebElement buttonLogin = driver.findElement(By.cssSelector("button[type='button']"));
//
//        inputLoginEmail.sendKeys(username);
//        inputLoginPassword.sendKeys(password);
//        buttonLogin.click();
    }

    @Then("^The error (.+) is showed$")
    public void the_error_is_showed(String message) throws Throwable {
//        Thread.sleep(2000);
//        WebElement bodyMessage = driver.findElement(By.cssSelector("div[class='body-message']"));
//        String msg = bodyMessage.getText();
//        System.out.println(message);
//        System.out.println(msg);
//        Assert.assertEquals(message, msg);
    }

    @After
    public void finish(Scenario scenario) {
//        scenario.embed();
//        driver.close();
    }
}
