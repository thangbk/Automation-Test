package automation.sikuli;

import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions {

    private WebDriver driver;

    @Before
    public void init() {
//        System.setProperty("webdriver.chrome.driver", "/Users/ThangBK/Downloads/chromedriver");
//        driver = new ChromeDriver();
    }

    public void sikuli() throws FindFailed
    {
        Screen screen = new Screen();
        Pattern pattern = new Pattern("/Users/ThangBK/Documents/Programming/Java/Workspace/Automation/src/test/resources/images/vscode.png");
        screen.click(pattern);
    }

    @Given("^given$")
    public void given() throws Throwable {
        sikuli();
    }

    @When("^when$")
    public void when() throws Throwable {
    }

    @Then("^Then$")
    public void then() throws Throwable {
    }

    @After
    public void finish(Scenario scenario) {
//        scenario.embed();
//        driver.close();
    }
}
