package automation.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class DriverManager {
    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public enum Browser {
        FIREFOX,
        CHROME,
        IE,
        ANDROID,
        IOS
    }

    private static WebDriver createInstance(Browser browser) throws MalformedURLException {
        WebDriver driver;
//        browserName = (browserName != null) ? browserName : "chrome";
        String version, deviceName;
        switch (browser) {
            case ANDROID:
                System.setProperty("webdriver.chrome.driver", "/Users/ThangBK/Downloads/chromedriver81");
                version = System.getProperty("version", "9.0");
                deviceName = System.getProperty("device", "17b3caff");
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, version);
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
                capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
                capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.CHROME);
                URL url = new URL("http://127.0.0.1:4723/wd/hub");

                driver = new AndroidDriver(url, capabilities);
                break;
            case IOS:
                version = System.getProperty("version", "13.4.5");
                deviceName = System.getProperty("device", "ThangBK's iPhone");
                DesiredCapabilities iosCap = new DesiredCapabilities();
                iosCap.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
                iosCap.setCapability(MobileCapabilityType.PLATFORM_VERSION, version);
                iosCap.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
                iosCap.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.SAFARI);
                iosCap.setCapability(MobileCapabilityType.UDID, "00008020-000879300130003A");
                iosCap.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
                iosCap.setCapability("xcodeOrgId", "SM2MN9J536");
                iosCap.setCapability("xcodeSigningId", "Apple Development: Thang Nguyen Manh (WYX2F58A6H)");
//                iosCap.setCapability("xcodeSigningId", "iPhone Developer");

                URL iosUrl = new URL("http://127.0.0.1:4723/wd/hub");

                driver = new IOSDriver(iosUrl, iosCap);
                break;
            case IE:
                driver = new InternetExplorerDriver();
                break;
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", "/Users/ThangBK/Downloads/geckodriver");
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                break;
            default:
                System.setProperty("webdriver.chrome.driver", "/Users/ThangBK/Downloads/chromedriver83");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                break;
        }
        return driver;
    }

    public static WebDriver getDriver(Browser browser) {
        if (driver.get() == null) {
//            String browserName = System.getProperty("browser", "chrome");
            try {
                setWebDriver(createInstance(browser));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return driver.get();
    }

    public static void setWebDriver(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        DriverManager.driver.set(driver);
    }

    public static void closeWebDriver()
    {
        driver.get().close();
        driver.get().quit();
        driver.remove();
    }
}