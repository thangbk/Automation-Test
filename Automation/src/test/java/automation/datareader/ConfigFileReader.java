package automation.datareader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import automation.driver.DriverManager.Browser;

public class ConfigFileReader {
    private final Properties properties;

    public ConfigFileReader(String propertyFilePath){
        try(BufferedReader reader = new BufferedReader(new FileReader(propertyFilePath)))
        {
            properties = new Properties();
            try
            {
                properties.load(reader);
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
        catch(Exception e)
        {
            throw new RuntimeException("Properties file not found at path : " + propertyFilePath);
        }
    }

    public String getDriverPath(){
        String driverPath = properties.getProperty("driverPath");
        if(driverPath!= null) return driverPath;
        else throw new RuntimeException("Driver Path not specified in the Configuration.properties file");
    }

    public long getImplicitlyWait() {
        String implicitlyWait = properties.getProperty("implicitlyWait");
        if(implicitlyWait != null) {
            try{
                return Long.parseLong(implicitlyWait);
            }catch(NumberFormatException e) {
                throw new RuntimeException("Not able to parse value : " + implicitlyWait + " in to Long");
            }
        }
        return 10;
    }

    public Browser getBrowser() {
        String browserName = properties.getProperty("browser");
        if(browserName == null || browserName.equalsIgnoreCase("chrome")) return Browser.CHROME;
        else if(browserName.equalsIgnoreCase("firefox")) return Browser.FIREFOX;
        else if(browserName.equalsIgnoreCase("ie")) return Browser.IE;
        else if(browserName.equalsIgnoreCase("ios")) return Browser.IOS;
        else if(browserName.equalsIgnoreCase("android")) return Browser.ANDROID;
        else throw new RuntimeException("Browser name is not matched : " + browserName);
    }

    public Boolean getBrowserWindowSize() {
        String windowSize = properties.getProperty("windowMaximize");
        if(windowSize != null) return Boolean.valueOf(windowSize);
        return true;
    }

    public String getDataResourcePath(){
        String testDataResourcePath = properties.getProperty("testDataResourcePath");
        if(testDataResourcePath!= null) return testDataResourcePath;
        else throw new RuntimeException("Test Data Resource Path not specified in the Configuration.properties file for the Key:testDataResourcePath");
    }

}
