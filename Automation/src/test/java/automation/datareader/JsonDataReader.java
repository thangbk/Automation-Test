package automation.datareader;

import com.google.gson.Gson;

import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

public class JsonDataReader<T> {

    public JsonDataReader() {}

    public List<T> loadData(String dataFilePath, Class<T[]> clazz)
    {
        Gson gson = new Gson();
        try(FileReader reader = new FileReader(dataFilePath)) {
            T[] arr = gson.fromJson(reader, clazz);
            return Arrays.asList(arr);
        }
        catch(Exception e) {
            throw new RuntimeException("Json file not found: " + dataFilePath);
        }
    }
}