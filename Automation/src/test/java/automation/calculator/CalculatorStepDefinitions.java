package automation.calculator;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.util.List;

import automation.datareader.ConfigFileReader;
import automation.datareader.JsonDataReader;
import automation.driver.DriverManager;
import automation.driver.DriverManager.Browser;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CalculatorStepDefinitions
{
    private final CalculatorPO calculatorPage;
    private final List<CalculatorModel> models;

    public CalculatorStepDefinitions() {
        String configFile = System.getProperty("config", "/Users/ThangBK/Documents/Programming/Java/Workspace/Automation-Test/Automation/src/test/resources/config/config.ios.properties");
        ConfigFileReader config = new ConfigFileReader(configFile);
        Browser browser = config.getBrowser();
        WebDriver driver = DriverManager.getDriver(browser);
        calculatorPage = new CalculatorPO(driver);
        JsonDataReader<CalculatorModel> jsonDataReader = new JsonDataReader<>();
        models = jsonDataReader.loadData(config.getDataResourcePath(), CalculatorModel[].class);
    }

    @Given("Visit page desmos.com scientific")
    public void visit_page_desmoscomscientific() {
        calculatorPage.open();
        calculatorPage.initElements();
    }

    @When("^Calculator with data load from file$")
    public void calculator_with_data_load_from_file()
    {

        for(CalculatorModel model: models) {
            String number1 = model.getNumber1();
            String number2 = model.getNumber2();
            String operator = model.getOperator();
            String expectResult = model.getResult();
            calculatorPage.enterElement(number1);
            calculatorPage.enterElement(operator);
            calculatorPage.enterElement(number2);
            calculatorPage.initElements();
//            calculatorPage.implicitlyWait();
            calculatorPage.waitUtilResult();
            String result = calculatorPage.getResult();
            Assert.assertEquals(String.format("Failed: %s %s %s", number1, operator, number2) , expectResult, result);
            calculatorPage.enterKey("clearall");
//            calculatorPage.implicitlyWait();
//            Thread.sleep(2000);
        }
    }

    @Then("^Show report$")
    public void show_report() {
        System.out.println("Report");
    }

    @After
    public void finish() {
        calculatorPage.close();
    }
}
