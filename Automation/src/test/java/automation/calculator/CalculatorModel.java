package automation.calculator;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "operator",
        "number1",
        "number2",
        "result"
})
public class CalculatorModel {

    @JsonProperty("operator")
    private String operator;
    @JsonProperty("number1")
    private String number1;
    @JsonProperty("number2")
    private String number2;
    @JsonProperty("result")
    private String result;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("operator")
    public String getOperator() {
        return operator;
    }

    @JsonProperty("operator")
    public void setOperator(String operator) {
        this.operator = operator;
    }

    @JsonProperty("number1")
    public String getNumber1() {
        return number1;
    }

    @JsonProperty("number1")
    public void setNumber1(String number1) {
        this.number1 = number1;
    }

    @JsonProperty("number2")
    public String getNumber2() {
        return number2;
    }

    @JsonProperty("number2")
    public void setNumber2(String number2) {
        this.number2 = number2;
    }

    @JsonProperty("result")
    public String getResult() {
        return result;
    }

    @JsonProperty("result")
    public void setResult(String result) {
        this.result = result;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}