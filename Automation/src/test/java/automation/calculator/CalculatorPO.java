package automation.calculator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CalculatorPO
{
    private final WebDriverWait wait;

    private final By byResultElmts = By.cssSelector("div.dcg-exp-output-container span.dcg-mq-root-block span");

    @FindAll(
        @FindBy(css = "div.dcg-exp-output-container span.dcg-mq-root-block span")
    )
    public List<WebElement> resultElmts;
    @FindAll(
        @FindBy(css = "div.dcg-exp-output-container var")
    )
    public List<WebElement> invalidResultElmts;
    private final WebDriver driver;


    public CalculatorPO(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 5);
    }

    public void enterElement(String element) {
        char[] chars = element.toCharArray();
        for (char ch: chars) {
            WebElement button = driver.findElement(By.cssSelector(String.format("span[role='button'][dcg-command='%s']", ch)));
            if(button != null) {
                button.click();
            }
        }
    }

    public void enterKey(String key) {
        WebElement button = driver.findElement(By.cssSelector(String.format("div[role='button'][dcg-command='%s']", key)));
        if(button != null) {
            button.click();
        }
    }

    public String getResult() {
        initElements();
        StringBuilder result = new StringBuilder();
        for (WebElement e: resultElmts) {
            result.append(e.getText());
        }

//        for (WebElement e: invalidResultElmts) {
//            result.append(e.getText());
//        }

        return result.toString().replace("=", "");
    }

    public void initElements() {
        PageFactory.initElements(this.driver, this);
    }

    public void open() {
        driver.get("https://www.desmos.com/scientific");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    public void close() {
        driver.close();
    }

    public void waitUtilResult() {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(byResultElmts));
    }

    public void waitUtilClear() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(byResultElmts));
    }
}
