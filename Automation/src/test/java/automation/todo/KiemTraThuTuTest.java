package automation.todo;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty","html:target/report"},
        features = "src/test/resources/automation/todos.feature",
        glue = {"automation.todo"}
)
public class KiemTraThuTuTest
{
}


