package automation.todo;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import java.util.List;

import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions
{

    private final WebDriver driver;
    private List<String> keywords;

    public StepDefinitions() {
        driver = Hook.driver;
    }

    @Given("^Nhap cac tu vao db$")
    public void nhap_cac_tu_vao_db(List<String> keywords) throws Throwable {
        this.keywords = keywords;
    }

    @When("^Nhap cac tu vao web$")
    public void nhap_cac_tu_vao_web() throws Throwable {
        driver.get("http://todomvc.com/examples/react-backbone/");
        Thread.sleep(2000);
        WebElement element = driver.findElement(By.cssSelector("input[class='new-todo']"));
        for(String keyword: keywords) {
            element.sendKeys(keyword);
            element.sendKeys(Keys.ENTER);
        }
    }

    @And("^Refresh trang web$")
    public void refresh_trang_web() throws Throwable {
        driver.navigate().refresh();
        Thread.sleep(5000);
    }

    @Then("^Kiem tra thu tu$")
    public void kiem_tra_thu_tu() throws Throwable {

        int wrong = 0;
        List<WebElement> elements = driver.findElements(By.cssSelector("div[class='view']"));
        for (int i=0; i<elements.size(); i++) {
            WebElement e = elements.get(i);
            String label = e.getText();
            if(!keywords.get(i).equals(label)) {
                System.out.println("Sai thứ tự: keyword: "+ label + ", index: "+ i+1);
                wrong += 1;
            }
        }

        System.out.println("Sai "+ wrong);
    }

    public void sikuli() throws FindFailed
    {
        Screen screen = new Screen();
        Pattern pattern = new Pattern("src/test/resources/images/xcode.png");
        screen.click(pattern);
    }

    @After
    public void finish() {
        driver.close();
    }
}
