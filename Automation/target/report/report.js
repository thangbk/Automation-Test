$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/calculator.feature");
formatter.feature({
  "name": "",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "+ - * / calculator",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Visit page desmos.com scientific",
  "keyword": "Given "
});
formatter.match({
  "location": "automation.calculator.test.CalculatorStepDefinitions.visit_page_desmoscomscientific()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Calculator with data load from file",
  "keyword": "When "
});
formatter.match({
  "location": "automation.calculator.test.CalculatorStepDefinitions.calculator_with_data_load_from_file()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Show report",
  "keyword": "Then "
});
formatter.match({
  "location": "automation.calculator.test.CalculatorStepDefinitions.show_report()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});