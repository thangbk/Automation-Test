Feature:
  Scenario Outline: Show error message after login with invalid credentials
    Given The login screen is displayed on browser
    When The user attempt to login with <username> and invalid <password>
    Then The error <message> is showed
    Examples:
      | username          | password  |message                                    |
      | khanh.tx@live.com |1234       |The message must be more than 6 character  |
      | khanh.tx          |abc123     |The email is invalid                       |